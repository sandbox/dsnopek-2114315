<?php
/**
 * @file
 * A user provider which works over services.
 */

/**
 * Interface remote user providers.
 */
class UserAuthServicesProvider extends RemoteUserProvider {
  protected $connection;

  /**
   * Construct a new UserAuthServicesProvider object.
   */
  public function __construct($info, $connection = NULL) {
    parent::__construct($info);

    $this->connection = $connection ? $connection
      : user_auth_services_client_connection();
  }

  /**
   * Make a request to the server.
   *
   * Convenience function for calling $this->connection->callMethodArray().
   *
   * @param string $method
   *   The HTTP method, ex. GET, POST, PUT.
   * @param string $path
   *   The path on the server to make the request against.
   * @param array $arguments
   *   An array of arguments to pass to the server.
   *
   * @return array
   *   The array of data returned by the server.
   */
  protected function request($method, $path, array $arguments = array()) {
    if ($method == 'GET' && !empty($arguments)) {
      $path .= '?' . http_build_query($arguments, '', '&');
      $arguments = array();
    }
    return $this->connection->callMethodArray($path, array($method, $arguments));
  }

  /**
   * Processes a remote object after getting it from the server.
   *
   * There are some standard modifications we want to make to any remote object
   * after getting it back from the server - this method takes care of them.
   *
   * @param object $remote
   *   The object (or array) from the remote server.
   *
   * @return object $remote
   *   The object all modified and ready.
   */
  protected function processRemote($remote) {
    $remote = (object)$remote;

    // For backwards compatibility.
    if (!empty($remote->bbsupport_server_sites)) {
      $remote->user_auth_services_sites = $remote->bbsupport_server_sites;
    }

    // By default, this will be decoded from JSON as an object, but we really
    // want an associative array.
    if (!empty($remote->user_auth_services_sites)) {
      $remote->user_auth_services_sites = (array)$remote->user_auth_services_sites;
    }

    return $remote;
  }

  /**
   * Get a list of user based on the given parameters.
   *
   * @param array $parameters
   *   An associative array of parameters to pass to the server.
   *
   * @retur array
   *   An array of objects returned by the server.
   */
  protected function findUsers(array $parameters) {
    $request = array();
    foreach ($parameters as $name => $value) {
      $request["parameters[{$name}]"] = $value;
    }
    return $this->request('GET', 'user', $request);
  }

  /**
   * {@inheritdoc}
   */
  public function isReady() {
    return (bool)$this->connection;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate($form, &$form_state) {
    $form_values = $form_state['values'];

    try {
      return $this->processRemote($this->request('POST', 'user/checkLogin', array(
        'username' => $form_values['name'],
        'password' => $form_values['pass'],
        'site_name' => user_auth_services_client_site_name(),
      )));
    }
    catch (Exception $e) {
      watchdog('user_auth_services_client', 'Unable to authenticate %username: @message',
        array('%username' => $form_values['name'], '@message' => $e->getMessage()), WATCHDOG_ERROR);
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateUserAccountForm(&$form, &$form_state) {
    $account = !empty($form['#user']) ? $form['#user'] : NULL;
    $remote_id = !empty($account) && !empty($account->remote_user) ?
      $account->remote_user['remote_id'] : NULL;

    $name = $form_state['values']['name'];
    $mail = $form_state['values']['mail'];

    if (!empty($name)) {
      try {
        $accounts = $this->findUsers(array('name' => $name));
        if ((count($accounts) == 1 && $accounts[0]->uid != $remote_id) || count($accounts) > 1) {
          form_set_error('name', t('The name %name is already taken.', array('%name' => $name)));
        }
      }
      catch (Exception $e) {
        watchdog('user_auth_services_client', 'Unable to look up users with username %username: @message',
          array('%username' => $name, '@message' => $e->getMessage()), WATCHDOG_ERROR);
        form_set_error('name', t('Unable to validate username on remote server.'));
      }
    }

    if (!empty($mail)) {
      try {
        $accounts = $this->findUsers(array('mail' => $mail));
        if ((count($accounts) == 1 && $accounts[0]->uid != $remote_id) || count($accounts) > 1) {
          form_set_error('mail', t('The e-mail address %email is already taken.', array('%email' => $mail)));
        }
      }
      catch (Exception $e) {
        watchdog('user_auth_services_client', 'Unable to look up users with mail %mail: @message',
          array('%mail' => $mail, '@message' => $e->getMessage()), WATCHDOG_ERROR);
        form_set_error('mail', t('Unable to validate e-mail on remote server.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateUserPasswordForm(&$form, &$form_state) {
    if (!empty($form_state['values']['account'])) {
      $account = $form_state['values']['account'];

      // Get the remote user object represented by the current user and make
      // sure they have permission to log into this site.
      if ($remote = $this->getRemoteUser($account->remote_user['remote_id'])) {
        $site_name = user_auth_services_client_site_name();
        if (in_array('administrator', array_values((array)$remote->roles)) || !empty($remote->user_auth_services_sites[$site_name])) {
          // Everything is good! So, we return.
          return;
        }
      }

      // We failed to validate that the user should be allowed to login, so
      // we set an error on the form.
      $name = $form_state['values']['name'];
      form_set_error('name', t('Sorry, %name is not recognized as a user name or an e-mail address.', array('%name' => $name)));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId($remote) {
    return (string)$remote->uid;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getRemoteUser($remote_id) {
    try {
      return $this->processRemote($this->request('GET', 'user/' . $remote_id));
    }
    catch (Exception $e) {
      watchdog('user_auth_services_client', 'Unable get remote user with remote ID %remote_id: @message',
        array('%remote_id' => $remote_id, '@message' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }

  /**
   * Get the remote user ID for a particular username.
   *
   * @param string $name
   *   The username to look for.
   *
   * @return object
   *   An object representing the remote user; or NULL if not found.
   */
  public function getRemoteUserByName($name) {
    $accounts = $this->findUsers(array('name' => $name));
    return !empty($accounts) ? $this->getRemoteUser($accounts[0]->uid) : NULL;
  }

  /**
   * Get the remote user ID for a particular username.
   *
   * @param string $mail
   *   The email to look for.
   *
   * @return object
   *   An object representing the remote user; or NULL if not found.
   */
  public function getRemoteUserByMail($mail) {
    $accounts = $this->findUsers(array('mail' => $mail));
    return !empty($accounts) ? $this->getRemoteUser($accounts[0]->uid) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createRemoteUser($account, $edit) {
    $site_name = user_auth_services_client_site_name();

    try {
      $request = array(
        'name' => $account->name,
        'mail' => $account->mail,
        'pass' => !empty($edit['pass_plain']) ? $edit['pass_plain'] : $account->pass,
        'status' => 1,
        'notify' => 0,
        'user_auth_services_sites' => array($site_name => $site_name),
        // For backwards compatibility.
        'bbsupport_server_sites' => array($site_name => $site_name),
      );

      // Allow other modules to modify the request.
      user_auth_services_client_invoke('create_remote_user', $request, $account);

      return $this->processRemote($this->request('POST', 'user', $request));
    }
    catch (Exception $e) {
      watchdog('user_auth_services_client', 'Unable to register account %name on remote server: @message',
        array('%name' => $account->name, '@message' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function updateRemoteUser($account, $edit) {
    $remote_id = $account->remote_user['remote_id'];

    // Pull out the items to update.
    $request = array();
    foreach (array('name', 'mail') as $key) {
      if (isset($edit[$key])) {
        $request[$key] = $edit[$key];
      }
    }
    if (isset($edit['pass_plain'])) {
      $request['pass'] = $edit['pass_plain'];
    }

    // The global status comes from $account->remote_user['status']
    $request['status'] = $account->remote_user['status'];

    if (!empty($request)) {
      $path = 'user/' . $remote_id;

      try {
        // Merge new information with old information.
        $original = $this->getRemoteUser($remote_id);
        $request = array_merge(array(
          // Always required!
          'name'   => $original->name,
          'mail'   => $original->mail,
          'status' => $original->status,
          // Will get cleared if we don't pass it.
          'roles'  => $original->roles,
        ), $request);
        // Should work but doesn't always..  We get an 'Illegal choice' error.
        //$request = array_merge((array)$client->request('GET', $path), $request);

        // Rework the 'user_auth_services_sites' into the format that the service expects.
        $request['user_auth_services_sites'] = array();
        foreach ($original->user_auth_services_sites as $site_name) {
          $request['user_auth_services_sites'][$site_name] = $site_name;
        }

        // Allow other modules to modify the request.
        user_auth_services_client_invoke('update_remote_user', $request, $account);

        // For backwards compatibility, with old servers...
        $request['bbsupport_server_sites'] = $request['user_auth_services_sites'];

        // do the actual update
        $this->request('PUT', $path, $request);
      }
      catch (Exception $e) {
        watchdog('user_auth_services_client', 'Unable to update remote server for remote ID %remote_id: @message',
          array('%remote_id' => $remote_id, '@message' => $e->getMessage()), WATCHDOG_ERROR);
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Grant a remote user access to the current site.
   *
   * @param object $remote
   *   An object representing the remote user.
   *
   * @return bool
   *   TRUE if successful; or FALSE otherwise.
   */
  public function grantRemoteUserSiteAccess($remote) {
    $site_name = user_auth_services_client_site_name();

    // If the user doesn't have permission to this site, add it!
    $sites = !empty($remote->user_auth_services_sites) ? (array)$remote->user_auth_services_sites : array();
    if (!in_array($site_name, $sites)) {
      try {
        // The service submits user_profile_form rather than calling user_save() directly,
        // so we have to submit to the 'bbsupport_server_sites' category specially.
        $request = array(
          'category' => 'bbsupport_server_sites',
          'user_auth_services_sites' => array(),
        );

        // Build up the list of sites.
        $sites[] = $site_name;
        foreach ($sites as $temp) {
          $request['user_auth_services_sites'][$temp] = $temp;
        }

        // For backwards compatibility.
        $request['bbsupport_server_sites'] = $request['user_auth_services_sites'];

        // Do the actual request!
        $this->request('PUT', 'user/' . $remote->uid, $request);
      }
      catch (Exception $e) {
        watchdog('user_auth_services_client', 'Unable to authorize user %user to access this site: %message', array('%user' => $remote->name, '%message' => $e->getMessage()), WATCHDOG_ERROR);
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRemoteUser($remote_id) {
    if (variable_get('user_auth_services_client_delete_remote', TRUE)) {
      try {
        $this->request('DELETE', 'user/' . $remote_id);
      }
      catch (Exception $e) {
        watchdog('user_auth_services_client', 'Unable to delete user %name on the server: @message',
          array('%name' => $account->name, '@message' => $e->getMessage()), WATCHDOG_ERROR);
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateLocalUser($account, $remote) {
    // replace the 'name', 'mail' and 'created' with values from the server
    $changed = FALSE;
    foreach (array('name', 'mail', 'created') as $key) {
      if ($account->$key != $remote->$key) {
        $account->$key = $remote->$key;
        $changed = TRUE;
      }
    }

    // update the user's suspend/unsuspend status
    if ($account->remote_user['status'] != $remote->status) {
      $account->remote_user['status'] = $remote->status;
      $changed = TRUE;
    }

    // Allow other modules to modify the request. If any of the hooks returned
    // a truthy value, then it'll be in the $hook_result array.
    $hook_result = user_auth_services_client_invoke('update_local_user', $account, $remote);
    $changed = !empty($hook_result) ? TRUE : $changed;

    return $changed;
  }
}
