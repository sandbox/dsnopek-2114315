<?php

/**
 * @file
 * Admin pages for User Authentication via Services (Client).
 */

/**
 * Form constructor for admin settings form.
 *
 * @ingroup forms
 */
function user_auth_services_client_settings_form($form, &$form_state) {
  $client_admin_url = url('admin/structure/clients');
  $client_connections = clients_connection_load_all('drupal_services_rest_7');

  if (count($client_connections) > 0) {
    $client_options = array('' => t('- None -'));
    foreach ($client_connections as $connection) {
      $client_options[$connection->name] = $connection->label;
    }
    $form['user_auth_services_client_connection'] = array(
      '#title' => t('Client connection'),
      '#type' => 'select',
      '#options' => $client_options,
      '#required' => TRUE,
      '#description' => t('The <a href="!client_admin_url">client connection</a> to use for user authentication.',
        array('!client_admin_url' => url('admin/structure/clients'))),
      '#default_value' => variable_get('user_auth_services_client_connection', ''),
    );
  }
  else {
    $client_add_url = url('admin/structure/clients/connections/add/drupal_services_rest_7',
      array('query' => array('destination' => current_path())));
    drupal_set_message(t('No client connections are available! Please <a href="!client_add_url">add a new client connection</a> first.', array('!client_add_url' => $client_add_url)), 'error');
  }

  $form['user_auth_services_client_site_name'] = array(
    '#title' => t('Site name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('An unique name to identify this site on the server.'),
    '#default_value' => user_auth_services_client_site_name(),
  );

  $form['user_auth_services_client_delete_remote'] = array(
    '#title' => t('Delete remote users'),
    '#type' => 'checkbox',
    '#description' => t('If enabled, this will delete remote users when the local account is deleted.'),
    '#default_value' => variable_get('user_auth_services_client_delete_remote', TRUE),
  );

  return system_settings_form($form);
}

