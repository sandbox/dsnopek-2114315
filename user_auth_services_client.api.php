<?php
/**
 * @file
 * API documentation for User Authentication via Services (Client)'s hooks.
 */

/**
 * Alter the request to create the remote user before it's sent.
 *
 * This is useful if you have a custom field on both the client and
 * server that you want to keep in sync.
 *
 * @param array $request
 *   The request that's about to be sent via 'POST user' to the server.
 * @param object $account
 *   A Drupal user object representing the local user.
 */
function hook_user_auth_client_services_create_remote_user(&$request, $account) {
  // Copy our custom field on the local user, to the equivalent field on the
  // remote user.
  $request['field_awesome'] = $account->field_awesome;
}

/**
 * Alter the request to update the remote user before it's sent.
 *
 * This is useful if you have a custom field on both the client and
 * server that you want to keep in sync.
 *
 * @param array $request
 *   The request that's about to be sent via 'POST user' to the server.
 * @param object $account
 *   A Drupal user object representing the local user.
 */
function hook_user_auth_client_services_update_remote_user(&$request, $account) {
  // Copy our custom field on the local user, to the equivalent field on the
  // remote user.
  $request['field_awesome'] = $account->field_awesome;
}

/**
 * Alter the local user using date from the remote user.
 *
 * This is useful if you have a custom field on both the client and
 * server that you want to keep in sync.
 *
 * @param object $account
 *   A Drupal user object representing the local user.
 * @param object $remote
 *   An object representing the remote user.
 *
 * @return boolean
 *   Return TRUE if you actually modified the local user. This will cause
 *   the account object to get saved.
 */
function hook_user_auth_client_services_update_local_user(&$account, $remote) {
  // Copy our custom field from the remote user to the local user.
  $account->field_awesome = $remote->field_awesome;

  return TRUE;
}
