<?php

/**
 * @file
 * Module file for User Authentication via Services (Client).
 */

/**
 * Implements hook_menu().
 */
function user_auth_services_client_menu() {
  $items = array();
  $items['admin/config/people/user_auth_services_client'] = array(
    'title' => 'User authentication via services (client)',
    'description' => 'Settings for user authentications via services (client)',
    'access arguments' => array('administer user_auth_services_client'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_auth_services_client_settings_form'),
    'file' => 'user_auth_services_client.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function user_auth_services_client_permission() {
  return array(
    'administer user_auth_services_client' => array(
      'title' => t('Administer user authentication via services (client)'),
    ),
  );
}

/**
 * Implements hook_remote_provider_info().
 */
function user_auth_services_client_remote_user_provider_info() {
  return array(
    'user_auth_services' => array(
      'title' => t('Services'),
      'class' => 'UserAuthServicesProvider',
    ),
  );
}

/**
 * Implements hook_user_insert().
 */
function user_auth_services_client_user_insert(&$edit, $account, $category) {
  if (!empty($account->remote_user['remote']) && $account->remote_user['provider'] = 'user_auth_services') {
    if ($provider = remote_user_get_provider('user_auth_services')) {
      // If a local user is created from a remote user, then we make sure that
      // it has access to this site.
      if (!$provider->grantRemoteUserSiteAccess($account->remote_user['remote'])) {
        drupal_set_message(t('Unable to authorize user %user to access this site.', array('%user' => $account->name)));
      }
    }
  }
}

/**
 * Get the client connection for user authentication.
 *
 * @return clients_connection_base
 *   The connection object or NULL if it hasn't been configured.
 */
function user_auth_services_client_connection() {
  if ($name = variable_get('user_auth_services_client_connection', '')) {
    return clients_connection_load($name);
  }
}

/**
 * Get the name of the current site.
 *
 * @return string
 *   A string uniquely identifying this site on the server.
 */
function user_auth_services_client_site_name() {
  $site_name = variable_get('user_auth_services_client_site_name', NULL);
  if (empty($site_name)) {
    if (preg_match('/sites\/(.*)$/', conf_path(), $matches)) {
      $site_name = $matches[1];
    }
  }
  if ($site_name == 'default') {
    return $_SERVER['SERVER_NAME'];
  }
  return $site_name;
}

/**
 * Invokes a user_auth_services_client hook in every module.
 *
 * @param string $type
 *   The type of the hook. Add 'hook_user_auth_services_client_' to the $type
 *   and you'll get the full name of the hook being called.
 * @param &$arg1
 *   The first argument is always alterable.
 * @param $arg2
 *   The second argument is not alterable.
 *
 * @return array
 *   An array containing all the return values which aren't falsy.
 */
function user_auth_services_client_invoke($type, &$arg1, $arg2) {
  $return = array();
  foreach (module_implements('user_auth_services_client_' . $type) as $module) {
    $function = $module . '_user_auth_services_client_' . $type;
    if ($result = $function($arg1, $arg2)) {
      $return[] = $result;
    }
  }
  return $return;
}
